﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShittyAnimationSprite : MonoBehaviour {

    public float delayBetweenFrames;
    public AudioSource audioSource;
    public AudioClip knife;
    [Range(0, 1)] public float knifeVol;
    public AudioClip ovenPing;
    [Range(0, 1)] public float ovenPingVol;



    public Image frame1;
    public Image frame2;
    public Image frame3;
    public Image frame4;
    public Image frame5;
    public Image frame6;
    public Image frame7;
    public Image frame8;
    public Text credits;
    public Text pressSpace;
    public Text pun;

    // Use this for initialization
    void Start () {
        frame1.enabled = true;
        frame2.enabled = false;
        frame3.enabled = false;
        frame4.enabled = false;
        frame5.enabled = false;
        frame6.enabled = false;
        frame7.enabled = false;
        frame8.enabled = false;
        credits.enabled = false;
        pun.enabled = false;
        pressSpace.enabled = false;
        Invoke("Frame2", delayBetweenFrames *4);
    }
    void Frame2()
    {
        frame2.enabled = true;
        Invoke("Frame3", delayBetweenFrames);
    }
    void Frame3()
    {
        frame3.enabled = true;
        Invoke("Frame4", delayBetweenFrames);
    }
    void Frame4()
    {
        frame4.enabled = true;
        audioSource.PlayOneShot(knife, knifeVol);
        Invoke("Frame5", delayBetweenFrames * 8);
    }
    void Frame5()
    {
        frame5.enabled = true;
        Invoke("Frame6", delayBetweenFrames);
    }
    void Frame6()
    {
        frame6.enabled = true;
        Invoke("Frame7", delayBetweenFrames);
    }
    void Frame7()
    {
        frame7.enabled = true;
        Invoke("Frame8", delayBetweenFrames);
    }
    void Frame8()
    {
        frame8.enabled = true;
        audioSource.PlayOneShot(ovenPing, ovenPingVol);
        Invoke("Credits", delayBetweenFrames * 4);
    }
    void Credits()
    {
        credits.enabled = true;
        pressSpace.enabled = true;
        pun.enabled = true;
    }
}
