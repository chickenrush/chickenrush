﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathExcluderPlayerScript : MonoBehaviour {

    public List<GameObject> exclusionList;

    private void FixedUpdate()
    {
        for (int i = 0; i < exclusionList.Count; i++)
        {
            exclusionList[i].GetComponent<LocationManager>().isExcluded = true;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Chicken Location" && other.GetComponent<LocationManager>().isExcluded == false)
        {
            exclusionList.Add(other.gameObject);
            //other.GetComponent<LocationManager>().isExcluded = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Chicken Location")
        {
            exclusionList.Remove(other.gameObject);
            other.GetComponent<LocationManager>().isExcluded = false;
        }
    }
}
