﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChickenController : MonoBehaviour {

    public ChickenManager manager;

    public enum ChickenBehaviourState  {
            //roaming,
            //startled,
            onTheRun,
            caught,
            //escaping
        }
    public ChickenBehaviourState chickState;
    public Transform caughtHoldTransformTemp;
    public Collider chickenCollider;
    public bool canBeCaught;

    //These things lerp the chicken to the right position
    public float lerpTime;
    public float lerpsSpeedMulti;

    private void Start()
    {
        manager = GetComponent<ChickenManager>();
        if (manager == null)
        {
            Debug.Log(name + "did not find it's manager");
        }
    }

    private void Update()
    {
        if (chickState == ChickenBehaviourState.caught)
        {
            if (transform.localPosition != Vector3.zero)
            {
                transform.localPosition = Vector3.Lerp(transform.localPosition, Vector3.zero, lerpTime);
                lerpTime += Time.deltaTime * lerpsSpeedMulti;
            }
        }  else
        {
            lerpTime = 0.0f;
        }
    }

    public bool GotCaught(Transform chickenHolderPos)
    {
        if (canBeCaught == true)
        {
            chickState = ChickenBehaviourState.caught;
            manager.particles.BurstFeathers();
            transform.parent = chickenHolderPos;
            canBeCaught = false;
            return true;
        } else
        {
            return false;
        } 
    }
    public void Released()
    {
        chickState = ChickenBehaviourState.onTheRun;
        transform.parent = null;
        canBeCaught = true;
    }
}
