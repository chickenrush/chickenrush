﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseScript : MonoBehaviour {

    public GameManager manager;

    public Image pauseBackground;
    public Image controlsImage;
    public Text pressSpaceTocontiue;

    public bool isPaused;

    void Start()
    {
        Unpause();
        Pause();
    }

	void Update () {

        if (!isPaused)
        {
            if (Input.GetKeyUp(manager.GMControls.pause))
            {
                Pause();
            }
        }
        if (isPaused == true)
        {
            if (Input.GetKeyDown(manager.GMControls.pauseQuit))
            {
                Application.Quit();
            }
            if (Input.GetKeyDown(manager.GMControls.pauseContinue))
            {
                Unpause();
            }
        }
	}

    public void Pause()
    {
        pauseBackground.enabled = true;
        controlsImage.enabled = true;
        pressSpaceTocontiue.enabled = true;
        Time.timeScale = 0.0f;
        isPaused = true;
    }

    public void Unpause()
    {
        Time.timeScale = 1.0f;
        pauseBackground.enabled = false;
        controlsImage.enabled = false;
        pressSpaceTocontiue.enabled = false;
        isPaused = false;
    }
}
