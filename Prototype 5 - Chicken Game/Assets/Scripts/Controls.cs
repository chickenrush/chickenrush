﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Controls : MonoBehaviour {

    public KeyCode pause;
    public KeyCode pauseQuit;
    public KeyCode pauseContinue;
    public KeyCode restart;
    [Header("Player 1 Controls")]
    public KeyCode up1;
    public KeyCode down1;
    public KeyCode left1;
    public KeyCode right1;
    public KeyCode mash1;
    [Header("Player 2 Controls")]
    public KeyCode up2;
    public KeyCode down2;
    public KeyCode left2;
    public KeyCode right2;
    public KeyCode mash2;

    private void Update()
    {
        if (Input.GetKeyDown(restart))
        {
            SceneManager.LoadScene(0);
        }
    }
}
