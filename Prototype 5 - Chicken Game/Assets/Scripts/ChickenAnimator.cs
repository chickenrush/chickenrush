﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChickenAnimator : MonoBehaviour {

    public ChickenManager manager;
    public Animator animator;

	void LateUpdate () {
		if (manager.controller.chickState == ChickenController.ChickenBehaviourState.onTheRun)
        {
            animator.SetBool("isRunning", true);
            animator.SetBool("isPanicing", false);
        }
        if (manager.controller.chickState == ChickenController.ChickenBehaviourState.caught)
        {
            animator.SetBool("isRunning", false);
            animator.SetBool("isPanicing", true);
        }
    }
}
