﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocationManager : MonoBehaviour {

    public bool debug;
    public bool isExcluded;
    public Material excluded;
    public Material notExcluded;
    public MeshRenderer meshRen;
	
	void Update () {
        if (debug)
        {
            meshRen.enabled = true;
            if (isExcluded)
            {
                meshRen.material = excluded;
            }
            else
            {
                meshRen.material = notExcluded;
            }
        } else
        {
            meshRen.enabled = false;
        }
		
	}
}
