﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ChickenAgentTestScript : MonoBehaviour {

    public ChickenManager manager;

    public NavMeshAgent agent;
    public Transform destination;
    public float newDestinationDistance;


    void Start ()
    {
        manager = GetComponent<ChickenManager>();
        if (manager == null)
        {
            Debug.Log(name + "did not find it's manager");
        }
        agent.obstacleAvoidanceType = ObstacleAvoidanceType.HighQualityObstacleAvoidance;
	}
	
	void Update () {
        if (manager.controller.chickState == ChickenController.ChickenBehaviourState.onTheRun)
        {
            agent.isStopped = false;
            if (destination != null)
            {
                if (Vector3.Distance(transform.position, destination.position) < newDestinationDistance || destination.GetComponent<LocationManager>().isExcluded == true)
                {
                    destination = null;
                }
            }
            if (destination == null)
            {
                destination = manager.gameManager.chickenDestinationPicker.NewDestination();
                agent.SetDestination(destination.position);
            }
        }

        if (manager.controller.chickState == ChickenController.ChickenBehaviourState.caught)
        {
            agent.isStopped = true;
        }

    }
}
