﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MashTest : MonoBehaviour {

    public PlayerManager manager;

    public bool runningMashGame;

    public Image left;
    public Image right;
    public Image background;

    public float maxMashNumber;
    public float mashNumber;

    public bool inputing;

	// Use this for initialization
	void Start () {
        background.enabled = false;
        left.enabled = false;
        right.enabled = false;
    }
	
	// Update is called once per frame
	void Update () {
        if (runningMashGame)
        {
            MashGame();
        }
    }

    public void StartMash()
    {
        background.enabled = true;
        left.enabled = true;
        right.enabled = true;
        manager.gameManager.GMEnder.PauseTimer();
        mashNumber = maxMashNumber / 2;
        runningMashGame = true;
    }

    public void EndMash()
    {
        background.enabled = false;
        left.enabled = false;
        right.enabled = false;
        manager.gameManager.GMEnder.ResumeTimer();
        runningMashGame = false;
        mashNumber = maxMashNumber / 2;
    }

    public void MashGame()
    {
        if (Input.GetKeyDown(manager.controls.mash))
        {
            inputing = true;
        }
        if (Input.GetKeyUp(manager.controls.mash))
        {
            inputing = false;
        }

        if (inputing)
        {
            mashNumber += Time.deltaTime;
        }
        else
        {
            mashNumber -= Time.deltaTime;
        }
        left.fillAmount = mashNumber / maxMashNumber;
        right.fillAmount = 1 - mashNumber / maxMashNumber;
        if (mashNumber <= 0 || mashNumber >= maxMashNumber)
        {
            manager.chickenGrabberM.RemoveChicken();
            EndMash();
        }
    }
}
