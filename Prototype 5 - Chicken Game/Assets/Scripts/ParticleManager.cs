﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleManager : MonoBehaviour {

    public ChickenManager manager;

    public ParticleSystem featherBurst;
    public ParticleSystem featherTrail;
    
	void Update () {
		/*if (manager.controller.chickState == ChickenController.ChickenBehaviourState.caught)
        {
            featherTrail.Play();
        } else
        {
            featherTrail.Pause();
        }*/
	}

    public void BurstFeathers()
    {
        featherBurst.Play();
    }
}
