﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChickenGrabberMain : MonoBehaviour {

    private PlayerManager manager;

    public bool runTimer;
    public float pickupChickTime; //I couldn't resist
    public float timerTime;
    public ChickenController chickenInTrigger;
    public Transform chickenHolder;

    private void Start()
    {
        manager = GetComponentInParent<PlayerManager>();
        if (manager == null)
        {
            Debug.Log(name + "'s Mover Script did not find Player Manager");
        }
    }

    private void Update()
    {
        if (runTimer)
        {
            timerTime += Time.deltaTime;
        }
        else
        {
            if (timerTime > 0)
            {
                timerTime -= Time.deltaTime;
            } else
            {
                timerTime = 0;
            }
        }
        if (timerTime >= pickupChickTime)
        {
            if (chickenInTrigger.GotCaught(chickenHolder))
            {
                manager.masher.StartMash();
                runTimer = false;
            }
            else
            {
                timerTime = 0.0f;
                runTimer = false;
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (chickenInTrigger != null)
        {
            if (chickenInTrigger.GetComponent<ChickenController>().chickState == ChickenController.ChickenBehaviourState.caught)
            {
                if (other.tag == "Home Zone")
                {
                    manager.ChickenIsHome();
                }
            }
        }
    }

    public void PassChicken(ChickenController chicken)
    {
        if (chicken.chickState != ChickenController.ChickenBehaviourState.caught)
        {
            chickenInTrigger = chicken;
            runTimer = true;
        }
    }

    public void RemoveChicken()
    {
        chickenInTrigger.Released();
        chickenInTrigger = null;
        runTimer = false;
    }
}
