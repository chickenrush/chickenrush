﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSmoother : MonoBehaviour {

    public Transform player1Trans;
    public Transform player2Trans;
    public Transform chickenTrans;
    public Transform trackedObject;

    public float maxMinX;
    public float maxMinZ;
    public float minYOffset;
    public float maxYOffset;
    public float minZOffset;
    public float maxZOffset;
    public float maxDistance;
    [Range(0, 1)]public float smoothTime;
    private Vector3 velocity = Vector3.zero;
    private Vector3 offset;


    void Update () {
        Vector3 trans1 = new Vector3(player1Trans.position.x, 0.0f, player1Trans.position.z);
        Vector3 trans2 = new Vector3(player2Trans.position.x, 0.0f, player2Trans.position.z);
        Vector3 trans3 = new Vector3(chickenTrans.position.x, 0.0f, chickenTrans.position.z);

        Vector3 centre = (trans1 + trans2 + trans3) / 3.0f;
        if (centre.x > maxMinX)
        {
            centre.x = maxMinX;
        } else if (centre.x < -maxMinX)
        {
            centre.x = -maxMinX;
        }
        if (centre.z > maxMinZ)
        {
            centre.z = maxMinZ;
        }
        else if (centre.z < -maxMinZ)
        {
            centre.z = -maxMinZ;
        }
        trackedObject.position = centre;

        float currentDist = Vector3.Distance(trans1, trans2); ;
        if (currentDist < Vector3.Distance(trans1, trans3))
        {
            currentDist = Vector3.Distance(trans1, trans3);
        } else if (currentDist < Vector3.Distance(trans2, trans3))
        {
            currentDist = Vector3.Distance(trans2, trans3);
        }
        float offsetMulti = currentDist / maxDistance;
        offset.y = ((maxYOffset - minYOffset) * offsetMulti) + minYOffset;
        offset.z = ((maxZOffset - minZOffset) * offsetMulti) + minZOffset;

    }

    private void FixedUpdate()
    {
        Vector3 desiredPos = trackedObject.position + offset;
        Vector3 smoothedPos = Vector3.SmoothDamp(transform.position, desiredPos, ref velocity, smoothTime);
        transform.position = smoothedPos;
    }
}
