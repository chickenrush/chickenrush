﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextFadeInOutScript : MonoBehaviour {

	Graphic theText;
    public Color fadeTo1;
    public Color fadeTo2;
	Color textColor;

	void Start () {
		theText = GetComponent<Graphic>();
		textColor = Color.white;
		theText.color = textColor;
	}


	// Update is called once per frame
	void Update () {
		textColor = Color.Lerp(fadeTo1, fadeTo2, Mathf.PingPong(Time.unscaledTime, 1));
		theText.color = textColor;
	}
}
