﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChickenDestinationPicker : MonoBehaviour {

    public List<Transform> destinationsList;

	// Use this for initialization
	void Start () {
        foreach (GameObject location in GameObject.FindGameObjectsWithTag("Chicken Location"))
        {
            destinationsList.Add(location.transform);
        }
	}

    public Transform NewDestination()
    {
        Transform temp = destinationsList[Random.Range(0, destinationsList.Count)];
        while (temp.GetComponent<LocationManager>().isExcluded == true)
        {
            temp = destinationsList[Random.Range(0, destinationsList.Count)];
        }
        
        return temp;
    }
}
