﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControls : MonoBehaviour {

    private PlayerManager manager;

    [HideInInspector] public KeyCode up;
    [HideInInspector] public KeyCode down;
    [HideInInspector] public KeyCode left;
    [HideInInspector] public KeyCode right;
    [HideInInspector] public KeyCode mash;
	
	void Start () {
        manager = GetComponent<PlayerManager>();
        if (manager == null)
        {
            Debug.Log(name + "'s Controls Script did not find Player Manager");
        }
        SetControls();
	}

    private void SetControls()
    {
        Controls controls = manager.gameManager.GMControls;

        if (manager.playID.playerNo == PlayerID.PlayerNo.Player1)
        {
            up = controls.up1;
            down = controls.down1;
            left = controls.left1;
            right = controls.right1;
            mash = controls.mash1;
            return;
        } else if (manager.playID.playerNo == PlayerID.PlayerNo.Player2)
        {
            up = controls.up2;
            down = controls.down2;
            left = controls.left2;
            right = controls.right2;
            mash = controls.mash2;
            return;
        } else
        {
            Debug.Log("Failed to set controls for " + name);
            return;
        }
    }
	
}
