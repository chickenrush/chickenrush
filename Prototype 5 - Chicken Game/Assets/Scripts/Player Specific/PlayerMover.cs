﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMover : MonoBehaviour
{

    private PlayerManager manager;
    private Rigidbody rb;

    public enum ActionState
    {
        idle,
        moving,
        turning,
    }
    public ActionState actionState;

    public float idleDrag;
    public float moveDrag;
    public float turnDrag;
    public float moveForceMin;
    public float moveForceMax;
    public float moveForceCurrent;
    public float moveForceRampSpeed;
    private bool callForce;
    private bool isRamping;
    private float rampTime;
    public float turnTimeRequirment;
    private float turnTimerTime = 0.0f;
    public float turnAngleSizeNormal;
    public float turnAngleSizeSharp;
    public float velocityToStopNormal;
    public float velocityToStopSharp;
    public bool isSharpTurn;

    private void Start()
    {
        manager = GetComponent<PlayerManager>();
        if (manager == null)
        {
            Debug.Log(name + "'s Mover Script did not find Player Manager");
        }
        rb = GetComponent<Rigidbody>();
        if (rb == null)
        {
            Debug.Log(name + "'s Mover Script did not find Rigidbody");
        }
        SetStateIdle();
    }

    private void Update()
    {
        if (actionState == ActionState.idle)
        {
            if (IsMovementInput())
            {
                SetStateMove();
            }
        }
        if (actionState == ActionState.moving)
        {
            Move(InputDirection());
        }
        if (actionState == ActionState.turning)
        {
            TurnStop();
        }
    }

    private void FixedUpdate()
    {
        if (callForce == true)
        {
            if (actionState == ActionState.moving || actionState == ActionState.idle)
            {
                rb.AddForce(InputDirection() * moveForceCurrent, ForceMode.Force);
            }
            callForce = false;
        }
    }

    private bool IsMovementInput()
    {
        if (Input.GetKey(manager.controls.up))
        {
            return true;
        }
        else if (Input.GetKey(manager.controls.down))
        {
            return true;
        }
        else if (Input.GetKey(manager.controls.left))
        {
            return true;
        }
        else if (Input.GetKey(manager.controls.right))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    public Vector3 InputDirection()
    {
        if (IsMovementInput() == false)
        {
            return Vector3.zero;
        }
        else
        {
            Vector3 dir = Vector3.zero;
            if (Input.GetKey(manager.controls.up))
            {
                dir += Vector3.forward;
            }
            if (Input.GetKey(manager.controls.down))
            {
                dir += Vector3.back;
            }
            if (Input.GetKey(manager.controls.left))
            {
                dir += Vector3.left;
            }
            if (Input.GetKey(manager.controls.right))
            {
                dir += Vector3.right;
            }
            return dir.normalized;
        }
    }
    public Vector3 CurrentDirection()
    {
        return rb.velocity.normalized;
    }
    public void SetStateIdle()
    {
        actionState = ActionState.idle;
        isSharpTurn = false;
        rb.drag = idleDrag;
    }
    public void SetStateMove()
    {
        actionState = ActionState.moving;
        isSharpTurn = false;
        rb.drag = moveDrag;
    }
    public void SetStateTurn()
    {
        actionState = ActionState.turning;
        rb.drag = turnDrag;
    }
    public void RampMoveForce()
    {
        if (rampTime < 1.0f)
        {
            moveForceCurrent = Mathf.Lerp(moveForceMin, moveForceMax, rampTime);
            rampTime += Time.deltaTime * moveForceRampSpeed;
        }
    }
    public void ResetMoveForce()
    {
        moveForceCurrent = moveForceMin;
        rampTime = 0.0f;
    }
    private void Move(Vector3 dir)
    {
        if (dir != Vector3.zero)
        {
            callForce = true;
            if (TurnStopGraceCounter())
            {
                SetStateTurn();
            }
            isRamping = true;
        } else
        {           
            if (TurnStopGraceCounter())
            {
                SetStateTurn();
            }
            isRamping = false;
        }
        if (isRamping == true)
        {
            RampMoveForce();
        }
        else
        {
            ResetMoveForce();
        }
    }
    private float CompareAngle()
    {
        return (Vector3.Angle(InputDirection(), CurrentDirection()));
    }
    private bool IsTurningOrStopping()
    {
        if (!IsMovementInput())
        {
            return true;
        } else
        if (CompareAngle() > turnAngleSizeNormal)
        {
            if (CompareAngle() > turnAngleSizeSharp)
            {
                isSharpTurn = true;
            }
            return true;
        } else
        {
            return false;
        }
    }
    private bool TurnStopGraceCounter()
    {
        if (IsTurningOrStopping())
        {
            turnTimerTime += Time.deltaTime;
        } else
        {
            turnTimerTime = 0;
        }
        if (turnTimerTime > turnTimeRequirment)
        {
            return true;
        } else
        {
            return false;
        }
    }
    private void ConcludingStop()
    {
        if (!isSharpTurn)
        {
            if (rb.velocity.magnitude < velocityToStopNormal)
            {
                SetStateIdle();
            }
            else
            {
                SetStateTurn();
            }
        } else
        {
            if (rb.velocity.magnitude < velocityToStopSharp)
            {
                SetStateIdle();
            }
            else
            {
                SetStateTurn();
            }
        }
        
    }
    private void TurnStop()
    {
        if (!IsTurningOrStopping())
        {
            actionState = ActionState.moving;
        }
        {
            ConcludingStop();
        }
    }
}
