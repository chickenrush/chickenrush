﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour {

    public GameManager gameManager;

    public MashTest masher;
    [HideInInspector] public PlayerControls controls;
    [HideInInspector] public PlayerID playID;
    [HideInInspector] public PlayerMover mover;
    [HideInInspector] public ChickenGrabberMain chickenGrabberM;
    [HideInInspector] public ChickenGrabberTrigger chickenGrabberTrig;

    private void Awake()
    {
        playID = GetComponent<PlayerID>();
        if (playID == null)
        {
            Debug.Log(name + " did not find Player ID");
        }
        controls = GetComponent<PlayerControls>();
        if (controls == null)
        {
            Debug.Log(name + " did not find Player Controls");
        }
        mover = GetComponent<PlayerMover>();
        if (mover == null)
        {
            Debug.Log(name + " did not find Player Mover");
        }
        chickenGrabberM = GetComponent<ChickenGrabberMain>();
        if (chickenGrabberM == null)
        {
            Debug.Log(name + " did not find the Main Chicken Grabber");
        }
        chickenGrabberTrig = GetComponentInChildren<ChickenGrabberTrigger>();
        if (chickenGrabberTrig == null)
        {
            Debug.Log(name + " did not find the Chicken Grabber Trigger Box");
        }
    }

    public void ChickenIsHome()
    {
        gameManager.GMEnder.EndGame(isWin: true);
    }
}
