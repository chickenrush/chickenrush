﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChickenGrabberTrigger : MonoBehaviour {

    private PlayerManager manager;

    private void Start()
    {
        manager = GetComponentInParent<PlayerManager>();
        if (manager == null)
        {
            Debug.Log(name + "'s Mover Script did not find Player Manager");
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Chicken")
        {
            if (manager.chickenGrabberM.chickenInTrigger == null)
            {
                manager.chickenGrabberM.PassChicken(other.GetComponent<ChickenController>());
            }
            else
            {
                Debug.Log("Dunno What to tell you... this ain't a chicken Georgie.");
            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (manager.chickenGrabberM.chickenInTrigger == other.GetComponent<ChickenController>())
        {
            if (other.GetComponent<ChickenController>().chickState != ChickenController.ChickenBehaviourState.caught)
            {
                manager.chickenGrabberM.RemoveChicken();
            }
        } 
        else
        {
            Debug.Log("Dunno What to tell you... that was not a chicken Georgie.");
        }
    }

}
