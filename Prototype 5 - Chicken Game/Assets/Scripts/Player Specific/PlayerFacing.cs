﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFacing : MonoBehaviour {

    private PlayerManager manager;

    public Transform facingDirector;
    public GameObject mesh;
    public float trackingSpeed;

    private void Start()
    {
        manager = GetComponent<PlayerManager>();
        if (manager == null)
        {
            Debug.Log(name + "'s Mover Script did not find Player Manager");
        }
    }

        void Update () {
        //mesh.transform.LookAt(facingDirector);
        Vector3 targetDir = facingDirector.localPosition - mesh.transform.localPosition;
        targetDir.y = 0.0f;
        mesh.transform.rotation = Quaternion.RotateTowards(mesh.transform.rotation, Quaternion.LookRotation(targetDir), Time.deltaTime * trackingSpeed);
        if (manager.mover.InputDirection() != Vector3.zero)
        {
            facingDirector.transform.localPosition = manager.mover.InputDirection();
        }
	}

}
