﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {

    public GameManager GM;
    public Text timerText;
    public string timerTextBeforeTime;

	
	// Update is called once per frame
	void Update () {
        timerText.text = timerTextBeforeTime + GM.GMEnder.timeRemainingInt;
	}
}
