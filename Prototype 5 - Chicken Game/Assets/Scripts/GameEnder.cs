﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameEnder : MonoBehaviour {

    public bool isPaused;

    public GameManager GM;
    public int maxGameLength;
    private float timeRemaining;
    public int timeRemainingInt;
    public int scoreMulti;

    private void Start()
    {
        timeRemaining = maxGameLength;
    }

    private void Update()
    {
        if (!isPaused)
        {
            timeRemaining -= Time.deltaTime;
            timeRemainingInt = Mathf.RoundToInt(timeRemaining);
            if (timeRemaining <= 0)
            {
                EndGame(isWin: false);
            }
        }
        
    }

    public void PauseTimer()
    {
        isPaused = true;
    }

    public void ResumeTimer()
    {
        isPaused = false;
    }

    public void EndGame(bool isWin)
    {
        if (isWin == true)
        {
            GM.score.gameScore = timeRemainingInt * scoreMulti;
            SceneManager.LoadScene(1);
            Debug.Log("The Players Won!");
        }
        else
        {
            SceneManager.LoadScene(2);
            Debug.Log("Womp Womp");
        }
    }
}
