﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndGameSceneManager : MonoBehaviour {

    public KeyCode quit;
    public KeyCode restart;

    public ScoreHolder score;
    public string scoreHolderTag;

	// Use this for initialization
	void Start () {
        score = GameObject.FindGameObjectWithTag(scoreHolderTag).GetComponent<ScoreHolder>();
	}

	void Update () {
		if (Input.GetKeyDown(quit))
        {
            Application.Quit();
        }
        if (Input.GetKeyDown(restart))
        {
            SceneManager.LoadScene(0);
        }
	}
}
