﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerUIManager : MonoBehaviour {

    public PlayerManager manager;
    public Image chickenGrabBarBk;
    public Image chickenGrabBar;
	
	void Update () {
		if (manager.chickenGrabberM.runTimer)
        {
            chickenGrabBarBk.enabled = true;
            chickenGrabBar.enabled = true;
            chickenGrabBar.fillAmount = manager.chickenGrabberM.timerTime / manager.chickenGrabberM.pickupChickTime;
        } else
        {
            chickenGrabBarBk.enabled = false;
            chickenGrabBar.enabled = false;
            chickenGrabBar.fillAmount = 0.0f;
        }
	}
}
