﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerNavObstacleManager : MonoBehaviour {

    public Transform chickenTrans;
    public float disableRange;
    public NavMeshObstacle playerObstacle;
    // Update is called once per frame
    void Update () {
		if (Vector3.Distance(transform.position, chickenTrans.position) > disableRange)
        {
            playerObstacle.enabled = true;
        } else
        {
            playerObstacle.enabled = false;
        }
	}
}
