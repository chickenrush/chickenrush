﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChickenManager : MonoBehaviour {

    public GameManager gameManager;
    public ChickenAgentTestScript agent;
    public ParticleManager particles;

    [HideInInspector] public ChickenController controller;

    private void Awake()
    {
        controller = GetComponent<ChickenController>();
        if (controller == null)
        {
            Debug.Log(name + " did not find the Chicken Controller");
        }
    }
}
