﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestinationRemover : MonoBehaviour {

    public ChickenManager manager;

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Hi");
        if (other.tag == "Chicken Location")
        {
            if (other == manager.agent.destination)
            {
                manager.agent.destination = null;
            }
        }
    }
}
