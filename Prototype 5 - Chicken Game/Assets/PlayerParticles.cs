﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerParticles : MonoBehaviour {

    public ParticleSystem dust;
    public PlayerManager manager;
    public bool isEmmiting;

    public void Update()
    {
        if (manager.mover.actionState == PlayerMover.ActionState.idle)
        {
            if (isEmmiting == true)
            {
                dust.Stop();
                isEmmiting = false;
            }
        } else
        {
            if (isEmmiting == false)
            {
                dust.Play();
                isEmmiting = true;

            }
        }
        
    }

}
